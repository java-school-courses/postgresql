package ru.java.school.courses.postresql.hibernatestarter;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import ru.java.school.courses.postresql.hibernatestarter.entity.Post;
import ru.java.school.courses.postresql.hibernatestarter.repository.StatementFactory;
import ru.java.school.courses.postresql.hibernatestarter.service.PostService;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class SQLInjectionTest2 {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private PostService postService;

    @Autowired
    private StatementFactory statementFactory;

    @BeforeAll
    void context() {
        //given 3 post
        postService.createPost(Post.builder().title("title1").build());
        postService.createPost(Post.builder().title("title2").build());
        postService.createPost(Post.builder().title("title3").build());
    }

    @Test
    public void injectDropTable() {
        String sql = "UPDATE post " +
                "SET title = 'new title1'" +
                "WHERE id = 1";
        statementFactory.executeQueryUsingPreparedStatement(entityManager, sql);

        sql = "UPDATE post " +
                "SET title = '" + "'; DROP TABLE post CASCADE; -- '" + "' " +
                "WHERE id = 1";
        statementFactory.executeQueryUsingPreparedStatement(entityManager, sql);

        sql = "UPDATE post " +
                "SET title = 'new title2'" +
                "WHERE id = 1";
        statementFactory.executeQueryUsingPreparedStatement(entityManager, sql);
    }
}
