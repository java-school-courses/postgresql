package ru.java.school.courses.postresql.hibernatestarter;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import ru.java.school.courses.postresql.hibernatestarter.entity.Post;
import ru.java.school.courses.postresql.hibernatestarter.service.PostService;

import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class FirstLevelCacheTest {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private PostService postService;

    @Test
    @Transactional
    public void whenExecuteFindByIdTwoTimes_thenDidOneSelectToDb() {
        //given 3 post
        postService.createPost(Post.builder().title("title1").build());
        postService.createPost(Post.builder().title("title2").build());
        postService.createPost(Post.builder().title("title3").build());
        //when\then
        Post post1 = entityManager.find(Post.class, 1l);
        assertEquals("title1", post1.getTitle());
        Post post2 = entityManager.find(Post.class, 1l);
        assertEquals("title1", post2.getTitle());
    }


}
