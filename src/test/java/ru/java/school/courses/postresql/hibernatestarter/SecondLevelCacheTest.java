package ru.java.school.courses.postresql.hibernatestarter;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import ru.java.school.courses.postresql.hibernatestarter.entity.Post;
import ru.java.school.courses.postresql.hibernatestarter.service.PostService;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class SecondLevelCacheTest {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private PostService postService;

    @BeforeAll
    void context() {
        //given 3 post
        postService.createPost(Post.builder().title("title1").build());
        postService.createPost(Post.builder().title("title2").build());
        postService.createPost(Post.builder().title("title3").build());
    }

    @Test
    @Transactional
    public void findFirst() {
        Post post = entityManager.find(Post.class, 1l);
        assertEquals("title1", post.getTitle());
    }

    @Test
    @Transactional
    public void findSecond() {
        Post post = entityManager.find(Post.class, 1l);
        assertEquals("title1", post.getTitle());
    }
}
