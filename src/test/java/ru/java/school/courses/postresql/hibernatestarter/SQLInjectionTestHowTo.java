package ru.java.school.courses.postresql.hibernatestarter;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import ru.java.school.courses.postresql.hibernatestarter.entity.Post;
import ru.java.school.courses.postresql.hibernatestarter.repository.StatementFactory;
import ru.java.school.courses.postresql.hibernatestarter.service.PostService;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class SQLInjectionTestHowTo {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private PostService postService;

    @Autowired
    private StatementFactory statementFactory;

    @BeforeAll
    void context() {
        //given 3 post
        postService.createPost(Post.builder().title("title1").build());
        postService.createPost(Post.builder().title("title2").build());
        postService.createPost(Post.builder().title("title3").build());
    }

    @Test
    public void injectDropTable() {
        String sql = "UPDATE post " +
                "SET title = ?" +
                "WHERE id = ?";
        statementFactory.executeQueryUsingPreparedStatementWithHolders(entityManager, sql, 1l, "new title");

        sql = "UPDATE post " +
                "SET title = ?" +
                "WHERE id = ?";
        statementFactory.executeQueryUsingPreparedStatementWithHolders(entityManager, sql, 1l, "'; DROP TABLE post CASCADE; -- '");


        sql = "UPDATE post " +
                "SET title = ?" +
                "WHERE id = ?";
        statementFactory.executeQueryUsingPreparedStatementWithHolders(entityManager, sql, 1l, "new title");

    }
}
