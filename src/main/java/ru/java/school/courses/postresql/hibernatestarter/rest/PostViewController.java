package ru.java.school.courses.postresql.hibernatestarter.rest;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.java.school.courses.postresql.hibernatestarter.view.PostCardView;
import ru.java.school.courses.postresql.hibernatestarter.view.PostListView;
import ru.java.school.courses.postresql.hibernatestarter.service.PostService;

@Slf4j
@RestController
@RequestMapping("/v1/forum")
@RequiredArgsConstructor
public class PostViewController {

    private final PostService postService;

    @GetMapping("/posts/{postId}")
    public PostCardView getPost(@PathVariable Long postId) {
        return postService.getPost(postId);
    }

    @GetMapping("/posts")
    public Page<PostListView> getPosts(Pageable page) {
        return postService.getPostListViewPage(page);
    }

}
