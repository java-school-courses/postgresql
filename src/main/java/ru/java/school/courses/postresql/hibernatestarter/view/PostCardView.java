package ru.java.school.courses.postresql.hibernatestarter.view;

import java.util.List;

public interface PostCardView {

    Long getId();

    String getTitle();

    List<CommentCardView> getComments();
}
