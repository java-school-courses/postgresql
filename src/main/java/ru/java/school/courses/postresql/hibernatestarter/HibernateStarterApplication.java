package ru.java.school.courses.postresql.hibernatestarter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HibernateStarterApplication {

	public static void main(String[] args) {
		SpringApplication.run(HibernateStarterApplication.class, args);
	}

}
