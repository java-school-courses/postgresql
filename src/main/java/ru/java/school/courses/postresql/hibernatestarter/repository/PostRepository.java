package ru.java.school.courses.postresql.hibernatestarter.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.java.school.courses.postresql.hibernatestarter.entity.Post;
import ru.java.school.courses.postresql.hibernatestarter.view.PostCardView;
import ru.java.school.courses.postresql.hibernatestarter.view.PostListView;

public interface PostRepository extends JpaRepository<Post, Long> {
    Page<PostListView> getPostsBy(Pageable pageable);

    PostCardView getPostsById(Long id);
}
