package ru.java.school.courses.postresql.hibernatestarter.exception;


public class NotFoundException extends RuntimeException {
    public NotFoundException(String message) {
        super(message);
    }
}
