package ru.java.school.courses.postresql.hibernatestarter.repository;

import org.hibernate.Session;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.sql.PreparedStatement;
import java.sql.Statement;

@Component
public class StatementFactory {

    @Transactional
    public void executeQueryUsingStatement(EntityManager entityManager, String query) {
        Session session = entityManager.unwrap(Session.class);
        session.doWork(connection -> {
            try (Statement statement = connection.createStatement()) {
                statement.executeUpdate(
                        query
                );
            }
        });
    }

    @Transactional
    public void executeQueryUsingPreparedStatement(EntityManager entityManager, String query) {
        Session session = entityManager.unwrap(Session.class);
        session.doWork(connection -> {
            try (PreparedStatement statement = connection.prepareStatement(
                    query
            )) {
                statement.executeUpdate();
            }
        });
    }

    @Transactional
    public void executeQueryUsingPreparedStatementWithHolders(EntityManager entityManager, String query, Long id, String title) {
        Session session = entityManager.unwrap(Session.class);
        session.doWork(connection -> {
            try (PreparedStatement statement = connection.prepareStatement(query)) {
                statement.setString(1, title);
                statement.setLong(2, id);
                statement.executeUpdate();
            }
        });
    }
}
