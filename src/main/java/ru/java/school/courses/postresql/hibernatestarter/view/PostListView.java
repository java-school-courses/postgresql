package ru.java.school.courses.postresql.hibernatestarter.view;

public interface PostListView {

    Long getId();

    String getTitle();
}
