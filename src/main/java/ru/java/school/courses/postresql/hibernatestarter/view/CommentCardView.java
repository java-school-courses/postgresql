package ru.java.school.courses.postresql.hibernatestarter.view;

import lombok.Data;
import lombok.NoArgsConstructor;

public interface CommentCardView {
    Long getId();

    String getData();
}
