package ru.java.school.courses.postresql.hibernatestarter.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import ru.java.school.courses.postresql.hibernatestarter.entity.Post;
import ru.java.school.courses.postresql.hibernatestarter.view.PostCardView;
import ru.java.school.courses.postresql.hibernatestarter.entity.PostComment;
import ru.java.school.courses.postresql.hibernatestarter.view.PostListView;
import ru.java.school.courses.postresql.hibernatestarter.exception.NotFoundException;
import ru.java.school.courses.postresql.hibernatestarter.repository.PostRepository;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
public class PostService {

    private final PostRepository postRepository;

    @Transactional
    public Long createPost(Post post) {
        List<PostComment> comments = post.getComments();
        if (comments != null) {
            comments.forEach(c -> c.setPost(post));
        }
        post.setCreatedDate(new Date());
        post.setCreatedDateInstant(Instant.now());
        post.setCreatedDateLocalDateTime(LocalDateTime.now());
        post.setCreatedDateTimestamp(Timestamp.from(Instant.now()));
        Post createdPost = postRepository.save(post);

        return createdPost.getId();
    }

    public Long createComment(Long postId, PostComment comment) {
        Optional<Post> post = postRepository.findById(postId);
        post.ifPresentOrElse(
                p -> {
                    p.addComment(comment);
                    postRepository.save(p);
                },
                () -> {
                    throw new NotFoundException("Ошибка добавления комментария: пост не найден");
                }
        );

        return comment.getId();
    }

    public Post findPost(Long postId) {
        return postRepository.findById(postId).orElseThrow(
                () -> new NotFoundException("Ошибка получения: пост не найден")
        );
    }

    public void deleteComment(Long postId, PostComment comment) {
        Optional<Post> post = postRepository.findById(postId);
        post.ifPresentOrElse(
                p -> {
                    p.removeComment(comment);
                    postRepository.save(p);
                },
                () -> {
                    throw new NotFoundException("Ошибка добавления комментария: пост не найден");
                }
        );
    }

    public Page<Post> getPosts(Pageable page) {
        return postRepository.findAll(page);
    }

    //
    public PostCardView getPost(Long id) {
        return postRepository.getPostsById(id);
    }

    public Page<PostListView> getPostListViewPage(Pageable page) {
        return postRepository.getPostsBy(page);
    }
}
