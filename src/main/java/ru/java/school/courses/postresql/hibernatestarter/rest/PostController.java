package ru.java.school.courses.postresql.hibernatestarter.rest;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.java.school.courses.postresql.hibernatestarter.entity.EntityRef;
import ru.java.school.courses.postresql.hibernatestarter.entity.Post;
import ru.java.school.courses.postresql.hibernatestarter.entity.PostComment;
import ru.java.school.courses.postresql.hibernatestarter.service.PostService;

@Slf4j
@RestController
@RequestMapping("/forum")
@RequiredArgsConstructor
public class PostController {

    private final PostService postService;

    @PostMapping("/posts")
    public EntityRef addPost(@RequestBody Post post) {
        Long id = postService.createPost(post);
        return new EntityRef(id);
    }

    @PostMapping("/posts/{postId}/comments")
    public EntityRef addComment(@PathVariable Long postId, @RequestBody PostComment comment) {
        Long id = postService.createComment(postId, comment);
        return new EntityRef(id);
    }

    @GetMapping("/posts/{postId}")
    public Post getPost(@PathVariable Long postId) {
        Post post = postService.findPost(postId);
        return post;
    }

    @GetMapping("/posts")
    public Page<Post> getPost(Pageable page) {
        return postService.getPosts(page);
    }

    @DeleteMapping("/posts/{postId}/comments/{commentId}")
    public void deleteComment(@PathVariable Long postId, @RequestBody PostComment comment) {
        postService.deleteComment(postId, comment);
    }

}
