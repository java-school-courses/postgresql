package ru.java.school.courses.postresql.hibernatestarter.entity;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class EntityRef {

    private final Long id;
}
